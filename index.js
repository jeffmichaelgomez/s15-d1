// Operator

// Assignment Operators
// Basic Assignment Operator (=)
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Addition Assignment Operator (+=)
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-= , *=, /=)

// Arithmetic Operators
let x = 1397;
let y = 7831;
let sum = x + y;
console.log("Result of addition operator: "+sum);

let difference = x-y;
let product = x*y;
let quotient= x/y;
let modulus= x%y;

console.log("Result of difference operator: "+difference);
console.log("Result of product operator: "+product);
console.log("Result of quotient operator: "+quotient);
console.log("Result of modulus operator: "+modulus);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of mdas operation: " + pemdas);

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


//Type Coercion
// - Type coercion is the automatic or implicit conversion of 
// values from one data type to another

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);


let juan = 'juan';
// Comparison Operators

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan')
console.log('juan' == juan)

// Inequality operator (!=)
// - checks whether the operands are not equal/have different content
/*
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);
*/
// Strict Equality Operator (===)
console.log("With Strict Equality Operator");
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict Inequality Operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical And Operator (&& - Double ampersand)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical Or Operator (|| - Double Pipe)
// Return true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical Not Operator (! - Exclamation Point)
// Returns the opposite value (Negates the value)
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

/* Relational Operators
	<  - Less than
	>  - Greater than
	<= - Less than or equal to
	>= - Greater than or equal to

	let a = 5;
	let relate = a <= 5;
	console.log(relate);
*/

// Selection Control Structures
// if, else if and else statement

let numG = -1;

// if Statement - executes a statement if a specified condition is true
/* Syntax:
	if(condition){
		statement/s;
	}
*/
if(numG < 0){
	console.log("Hello");
}

let numH = -1
// else if Clause - executes a statement if previous conditions are false and if the specified condition is true

if(numG > 0){
	console.log("Hello");
}
else if (numH == 0){
	console.log("World");
}
else if (numH > 0){
	console.log("Solar System");
}

// else Statement - executes a statement if all other conditions are false
if(numG > 0){
	console.log("Hello");
}
else if (numH == 0){
	console.log("World");
}
else{
	console.log("Try again");
}

/*
	Department A = 1 - 3
	Department B = 4 - 6
	Department C = 7 - 9
*/

let dept = 4;

if(dept >= 1 && dept <= 3){
	console.log("You're in Department A");
}
else if(dept >= 4 && dept <= 6){
	console.log("You're in Department B");
}
else if(dept >= 7 && dept <= 9){
	console.log("You're in Department C");
}
else{
	console.log("Department does not exist");
}

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}
	else if(windSpeed < 61){
		return "Tropical Depression detected.";
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected.";
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical storm detected.";
	}
	else{
		return "Typhoon detected.";
	}
}

// Retuns the string to the variable "message" that invoked it
message = determineTyphoonIntensity(66);
console.log(message);

// Truthy and Falsy
// In JS a "truthy" value is a value that is considered true when encountered in Boolean context
/*  Falsy values
 1. false
 2. 0
 3. -0
 4. ""
 5. null
 6. undefined
 7. NaN
*/

if (true){
	console.log('Truthy');
}

if(1){
	console.log('Truthy')
}

// falsy example
if (false){
	console.log('Falsy');
}

if (0){
	console.log('Falsy');
}

if (undefined){
	console.log('Falsy');
}

// Single statement execution
// Conditional Ternary Operator
/* Syntax
	(expression) ? ifTrue : ifFalse;
*/
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane'
	return 'You are under the age limit';
}


let age = parseInt(prompt ("What is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator Functions: " + legalAge + ", " + name)


let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is orange");
		break;
	case 'tuesday':
		console.log("The color of the day is yellow");
		break;
	case 'wednesday':
		console.log("The color of the day is green");
		break;
	case 'thursday':
		console.log("The color of the day is blue");
		break;
	case 'friday':
		console.log("The color of the day is indigo");
		break;
	case 'saturday':
		console.log("The color of the day is violet");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
}

function showIntensityAlert(windSpeed){
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
	}
	finally {
		alert('Intensity updates will show new alert.');
	}
}

showIntensityAlert(56);